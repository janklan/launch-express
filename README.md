# Launch Express exercise

## About

All code in this repository was created as an exercise task according to the specification provided by Haley Arnold on UpWork. 

The specification is as follows:
   
> As a part of our hiring process we would like you to build a short, two-page survey with five questions per page, and each question weighed from zero to five that a user can fill out and see a statistics summary of the average answers per question for all users.
>
> The user should go to the app, register with either email/password, Google, or Facebook (you may implement whichever of the 3 methods you are most comfortable with), fill out the 2-page questionnaire, and see a statistics breakdown of values per question of all entries filled in the form of a chart of your choice.
>
> In addition to normal users, there should also be administrator users who can complete the survey but also see an additional page that contains the list of users that registered but did not fill out the survey. This page is hidden for non-administrator users, and they may never access it.    
>
> The technologies you should use are:
> - MySQL for the Database
> - PHP7 and Symfony4 for the backend
> - jQuery 3 and Bootstrap 4 for the frontend
>
> The last page, the statistics breakdown, should just be a simple chart (whatever you can think of as sensible statistics for a survey), implemented in your charting library of choice (ChartJS is a good candidate, but you can use whatever you are most comfortable with).
>
> The emphasis here is not on the UI or fancy animations, but on designing and building a complete and simple solution that does one thing and does it well.
>
> If you have any questions, or if you feel the requirements are unclear, please do not hesitate to contact me and I or our CIO, Milos, will get back with you.

The project has been completed to match the specification. 

## Usage

If you have Docker running on your computer:
 
1. Check out the git repository
1. Open the application root directory in terminal
1. Run `docker-compose up` to launch the virtual server 
1. Go to [http://localhost:8080](http://localhost:8080) and log in with one of the following users:
    1. `admin@localhost` with password `admin`
    1. `user@localhost` with password `user`
    
### Other things to know
    
1. Run `docker-compose exec symfony bin/console doctrine:fixtures:load -n` to reload the initial data fixtures
1. Run `docker-compose exec symfony npm run build` to compile all static assets
1. Run `docker-compose exec symfony bin/phpunit` to run unit tests

## Notes

1. Total elapsed time is 7 hour 30 minutes.
1. The project is lacking essential things I would have done if this was not a hiring process exercise. Let's talk about them later if I advance to the next step of the process based on the code done thus far.
1. All code is my work, and I shall retain its full ownership. Any distribution or further usage exceeding the purpose of the hiring process is prohibited.

# Contact

1. Feel free to get in touch via http://upwork.com/fl/janklan
