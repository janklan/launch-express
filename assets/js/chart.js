const $ = require('jquery');
const Chart = require('chart.js');

$(document).ready(() => {
    $('canvas.chart').each((i, e) => {
        showChart(e);
    })
});

const chartHandlers = {
    radar: createRadarChart,
    bar: getBarChart,
}

function showChart(element) {

    if (!element || !$(element).data('source')) {
        return;
    }

    $.getJSON($(element).data('source'), (response) => {

        const chartType = $(element).data('type');

        if (chartHandlers.hasOwnProperty(chartType)) {
            const ctx = element.getContext('2d');
            const chartOptions = chartHandlers[chartType](response);
            new Chart(ctx, chartOptions);
        }
    })
}

function createRadarChart(response) {

    return {
        type: 'radar',
        options: {
            legend: {
                display: false
            },
            maintainAspectRatio: true,
            spanGaps: false,
            plugins: {
                filler: {
                    propagate: true
                }
            },
            elements: {
                line: {
                    tension: 0.3
                }
            },
            tooltips: {
                enabled: true,
                callbacks: {
                    label: function(tooltipItem, data) {

                        let count = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        return data.datasets[tooltipItem.datasetIndex].label + ' : ' + count + (count > 1 ? ' points' : 'point');
                    }
                }
            }
        },
        data: response
    };
}

function getBarChart(response) {
    return {
        type: 'bar',
        options: {
            legend: {
                display: false
            },
        },
        data: response
    };
}