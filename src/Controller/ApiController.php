<?php

namespace App\Controller;

use App\Entity\SurveyAnswer;
use App\Entity\SurveyQuestion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class ApiController extends AbstractController
{
    /**
     * @Route("/update-answer/{question}", name="update_answer", methods={"POST"})
     */
    public function answer(Request $request, EntityManagerInterface $entityManager, SurveyQuestion $question): Response
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException('This endpoint is only available via an AJAX call.');
        }

        $answer = $entityManager->getRepository(SurveyAnswer::class)->findOneBy([
            'user' => $this->getUser()->getId(),
            'question' => $question->getId(),
        ]);

        if (!$answer) {
            $answer = new SurveyAnswer();
            $answer->setQuestion($question);
            $answer->setUser($this->getUser());
        }

        $answer->setAnswer($request->request->get('answer'));

        $entityManager->persist($answer);
        $entityManager->flush();

        return $this->json(['result' => 'success']);
    }

    /**
     * @Route("/summary/popularity/{top<$\d+$>?5}", name="summary_popularity", methods={"GET"})
     */
    public function summarizePopularity(EntityManagerInterface $entityManager, ?int $top = 5): Response
    {
        return $this->json($entityManager->getRepository(SurveyQuestion::class)->getPopularity($top));
    }

    /**
     * @Route("/summary/radar", name="summary_radar", methods={"GET"})
     */
    public function radar(EntityManagerInterface $entityManager): Response
    {
        return $this->json($entityManager->getRepository(SurveyQuestion::class)->getRadarAverage());
    }
}
