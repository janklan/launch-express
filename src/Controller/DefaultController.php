<?php

namespace App\Controller;

use App\Entity\SurveyQuestion;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/{page<1|2>?1}", name="homepage")
     */
    public function index(EntityManagerInterface $entityManager, int $page): Response
    {
        return $this->render('default/index.html.twig', [
            'page' => $page,
            'survey' => $entityManager->getRepository(SurveyQuestion::class)->getAnswersOf($this->getUser(), $page),
        ]);
    }

    /**
     * @Route("/summary", name="summary")
     */
    public function summary(): Response
    {
        return $this->render('default/summary.html.twig');
    }

    /**
     * @Route("/users", name="users")
     */
    public function users(EntityManagerInterface $entityManager): Response
    {
        return $this->render('default/users.html.twig', [
            'users' => $entityManager->getRepository(User::class)->findAllWithoutSubmission(),
        ]);
    }
}
