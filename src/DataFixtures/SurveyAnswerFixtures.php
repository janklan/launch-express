<?php

namespace App\DataFixtures;

use App\Entity\SurveyAnswer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SurveyAnswerFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = $this->getReference('user');
        for ($i = 1; $i <= SurveyQuestionFixtures::COUNT; ++$i) {
            $answer = new SurveyAnswer();
            $answer
                ->setQuestion($this->getReference('question'.$i))
                ->setUser($user)
                ->setAnswer(random_int(0, 5))
            ;

            $manager->persist($answer);
        }

        // Add 100 other users to generate some data
        for ($userNo = 1; $userNo <= UserFixtures::COUNT; ++$userNo) {
            // We'll skip every fifth user
            if (0 === $userNo % 5) {
                continue;
            }
            $user = $this->getReference('user'.$userNo);

            for ($questionNo = 1; $questionNo <= SurveyQuestionFixtures::COUNT; ++$questionNo) {
                if (1 === random_int(1, 30)) { // One in 30 chance that this question will be skipped, making the survey incomplete
                    continue;
                }

                $answer = new SurveyAnswer();
                $answer
                    ->setQuestion($this->getReference('question'.$questionNo))
                    ->setUser($user)
                    ->setAnswer(random_int(0, 5))
                ;
                $manager->persist($answer);
            }
        }

        $manager->flush();
    }

    /** {@inheritdoc} */
    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            SurveyQuestionFixtures::class,
        ];
    }
}
