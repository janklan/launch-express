<?php

namespace App\DataFixtures;

use App\Entity\SurveyQuestion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SurveyQuestionFixtures extends Fixture
{
    public const COUNT = 10;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i <= self::COUNT; ++$i) {
            $question = new SurveyQuestion();
            $question
                ->setQuestion('Question '.$i)
                ->setPosition($i)
            ;
            $manager->persist($question);

            $this->addReference('question'.$i, $question);
        }

        $manager->flush();
    }
}
