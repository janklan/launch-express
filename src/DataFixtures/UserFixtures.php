<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const COUNT = 100;

    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $admin = new User();
        $admin->setEmail('admin@localhost');
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setPassword($this->passwordEncoder->encodePassword($admin, 'admin'));

        $manager->persist($admin);
        $this->addReference('admin', $admin);

        $user = $this->getUser();
        $manager->persist($user);
        $this->addReference('user', $user);

        // Add a bunch of other users to generate some data
        for ($i = 1; $i <= self::COUNT; ++$i) {
            $manager->persist($this->getUser());
            $this->addReference('user'.$i, $user);
        }

        $manager->flush();
    }

    private function getUser(): User
    {
        static $counter = 1;

        $user = new User();
        $user->setEmail('user'.($counter++).'@localhost');
        $user->setRoles(['ROLE_USER']);
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'user'));

        return $user;
    }
}
