<?php

namespace App\Repository;

use App\Entity\SurveyQuestion;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SurveyQuestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method SurveyQuestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method SurveyQuestion[]    findAll()
 * @method SurveyQuestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurveyQuestionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SurveyQuestion::class);
    }

    /**
     * @param User $user
     * @param int  $page
     *
     * @return mixed
     */
    public function getAnswersOf(User $user, int $page = 1): array
    {
        $pageLength = 5;
        $firstResult = ($pageLength * $page) - $pageLength;

        if ($firstResult < 0) {
            $firstResult = 0;
        }

        return $this->createQueryBuilder('q')
            ->select('a.id AS answer_id', 'a.answer', 'q.id AS question_id', 'q.question')
            ->leftJoin('q.surveyAnswers', 'a', Expr\Join::WITH, 'a.question = q.id AND a.user = :user')
            ->setParameter(':user', $user->getId())
            ->orderBy('q.position', 'ASC')
            ->setFirstResult($firstResult)
            ->setMaxResults($pageLength)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getPopularity(int $limit): array
    {
        $result = $this->createQueryBuilder('q')
            ->select('q.question', 'SUM(a.answer) AS answer')
            ->leftJoin('q.surveyAnswers', 'a')
            ->groupBy('q.id')
            ->orderBy('answer', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;

        $return = [
            'labels' => [],
            'datasets' => [
                [
                    'backgroundColor' => 'rgba(54, 162, 235, 0.5)',
                    'label' => 'Total points collected',
                    'data' => [],
                ],
            ],
        ];

        foreach ($result as $row) {
            $return['labels'][] = $row['question'];
            $return['datasets'][0]['data'][] = (float) $row['answer'];
        }

        return $return;
    }

    public function getRadarAverage(): array
    {
        $result = $this->createQueryBuilder('q')
            ->select('q.question', 'AVG(a.answer) AS avgAnswer')
            ->leftJoin('q.surveyAnswers', 'a')
            ->groupBy('q.id')
            ->orderBy('q.id')
            ->getQuery()
            ->getResult()
        ;

        $return = [
            'labels' => [],
            'datasets' => [
                [
                    'backgroundColor' => 'rgba(54, 162, 235, 0.5)',
                    'label' => 'Average rating',
                    'data' => [],
                ],
            ],
        ];
        foreach ($result as $row) {
            $return['labels'][] = $row['question'];
            $return['datasets'][0]['data'][] = (float) $row['avgAnswer'];
        }

        return $return;
    }
}
