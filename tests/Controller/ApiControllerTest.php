<?php

namespace App\Test\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;

class ApiControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function testSummarizePopularityAnonymous(): void
    {
        $client = $this->makeClient();
        $client->request('GET', '/api/summary/popularity');
        $this->assertStatusCode(302, $client); // Should redirect to login page
    }

    public function testSummarizePopularity(): void
    {
        $fixtures = $this->loadFixtureFiles([
            'fixtures/test/users.yaml',
        ]);

        $this->loginAs($fixtures['user'], 'main');
        $client = $this->makeClient();
        $client->request('GET', '/api/summary/popularity');
        $this->assertResponseIsSuccessful();
    }
}
